#include "ai_minmax_search.h"
#include "gomoku_ai.h"
#include "stdio.h"

typedef Node<Gomoku_ai, Move> GomokuAI;

template <typename B, typename M>
M Node<B, M>::move_list[15];
template <typename B, typename M>
Node<B, M>* Node<B, M>::next_free;

Move ai_move(Gomoku_ai b, int level, CbFun fun, void* cbFunData)
{
   return GomokuAI::ai_move(b, level, fun, cbFunData);
}
