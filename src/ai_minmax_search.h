#ifndef AI_MINMAX_SEARCH_H
#define AI_MINMAX_SEARCH_H

#include <stdlib.h>
#include <time.h>
#include <new>
#include <stdio.h>

#define NO_MOVE_LIST (0xFFFF)
#define NO_GRAD      (0xFFFF)
#define MIN_GRAD     (-1024)
#define MAX_GRAD     ( 1024)

enum CbResult {
   CONTINUE,
   HURRY_UP,
   STOP_NOW
};
typedef CbResult (*CbFun)(void*);


template <typename B, typename M> 
class Node
{
   static M move_list[15];
   static Node* next_free;
   Node* parent;           /* Link to previous position*/
   B     board;            /* Board representation*/
   int   l;                /* Level deep.*/
   int   rating;           /* Rating for this position*/
   int   boundA, boundB;   /* Node bounds for Alfa-Beta puring*/
   bool  done;
   typename B::iterator moveIdx;
   bool  isMine() { return !(l %2); }
   bool  isOpenent() { return (l %2); }

   public:
   Node(B& b, Node* p, int level, int bA = MIN_GRAD, int bB = MAX_GRAD)
      :board(b), parent(p), l(level), boundA(bA), boundB(bB)
   {
      done = false;
      rating = NO_GRAD;
   }

   Node* evaluate() 
   {
      rating = board.evaluate(l);
      done = true;
      return this;
   }

   Node* go_down() 
   {
      /* Check if there are any possible moves left if not evaluate and return
       * this node */
      if (moveIdx == board.end())
      {
         done = true;
         if (rating == NO_GRAD)
         {
            evaluate();
         }
         return this;
      }
      if (l == 0)
      {
         M m = *moveIdx;
      }

      Node* child = new(next_free++) Node(board, this, l+1, boundA, boundB);
      child->board.move(*moveIdx);
      child->moveIdx = child->board.begin();

      if (rating != NO_GRAD)
      {
         if (isMine() && rating > child->boundA)
         {
            child->boundA = rating;
         }
         if (isOpenent() && rating < child->boundB)
         {
            child->boundB = rating;
         }
      }
      return child;
   }

   Node* go_up()
   {
      next_free--;

      if (parent->done)
      {
         parent->moveIdx++;
         return parent;
      }
      if (l == 1)
      {
         /* Add some random values to level one nodes. This is done to avoid AI
          * always choosing same moves */
         rating += rand()%5 != 0?
            0 : rand()%2? -1: 1;
      }
      if (
            /* There is no rating yet */
            (parent->rating == NO_GRAD)
            ||
            /* It is our node, so we need max */
            (parent->isMine() &&    rating > parent->rating)
            ||
            /* It is not our node, so we need min */
            (parent->isOpenent() && rating < parent->rating)
         )
      { 
         move_list[parent->l] = *(parent->moveIdx);
         parent->rating = rating;

         /* Alpha/Beta cutoff stop processing if the current result proves that parent
          * can not afffect best result*/
         if (parent->isOpenent() && parent->rating < parent->boundA)
         {
            /*The current value of a mininimum perent node is already lower then
             * the alpha bound.  */
            parent->done = true;
         }
         if (parent->isMine() && parent->rating > parent->boundB)
         {
            /*The current value of a maximum parent node is already higher then
             * the beta bound.  */
            parent->done = true;
         }
      }
      parent->moveIdx++;
      return parent;
   }

   static M ai_move(B board, int level, CbFun cbfun, void* cbData)
   {
      next_free = (Node*)malloc((level+1)*sizeof(Node));
      Node* current = new((void*)next_free++) Node(board, NULL, 0);
      current->moveIdx = current->board.begin();
      int givenLevel = level;

      while (true)
      {
         if (current->l < 2)
         {
            switch(cbfun(cbData))
            {
               case CONTINUE:
                  break;
               case HURRY_UP:
                  level = givenLevel-2;
                  break;
               case STOP_NOW:
                  level = 0;
                  break;
            }
         }


         if (!current->done && current->l < level) 
         {
            current = current->go_down();
         }
         else if (!current->done && current->l >= level) 
         { 
            current = current->evaluate();
         }
         else if (current->l != 0)
         {
            current = current->go_up();
         }
         else
         {
            free(current);
            return move_list[0];
         }
      }
   }
}; 
#endif
