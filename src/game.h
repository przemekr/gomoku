CbResult callback(void* callBackData)
{
   App* app = (App*)callBackData;
   return app->elapsed_time() <500.0? CONTINUE:
          app->elapsed_time() <1000.0? HURRY_UP:
          STOP_NOW;
}

class GameView : public View
{
public:
   GameView(App& application): app(application),
      menu(80,  20, 180, 40,   "MENU", !flip_y),
      undo(220, 20, 320, 40,   "UNDO", !flip_y),
      animation(50)
   {
      downX = downY = 0;
      undo.background_color(red);
      menu.background_color(red);

      add_ctrl(undo);
      add_ctrl(menu);

      was_multi = false;
      was_move = false;
   }

   void on_key(int x, int y, unsigned key, unsigned flags)
   {
      if (key == 27 || key == 0x4000010e /* Android BACK key*/)
      {
         undo.status(true);
         on_ctrl_change();
      }
      if (key == 'z')
      {
         printf("zoom in\n");
         zoom_mtx *= agg::trans_affine_translation(-x, -y);
         zoom_mtx *= agg::trans_affine_scaling(1.1);
         zoom_mtx *= agg::trans_affine_translation(x, y);
         on_resize(0, 0);
         size *= zoom_mtx.scale();
         zoom_mtx.transform(&wshift, &hshift);
         app.force_redraw();

      }
      if (key == 'a')
      {
         printf("zoom out\n");
         zoom_mtx.reset();
         on_resize(0, 0);
         app.force_redraw();
      }
   }

   virtual void on_ctrl_change()
   {
      app.wait_mode(true);
      if (app.gomoku.endOfTheGame())
      {
         animation = 0;
      }
      if (app.gomoku.currentPlayer() == WHITE && app.p1level
            || app.gomoku.currentPlayer() == BLACK && app.p2level)
      {
         app.wait_mode(false);
      }

      if (undo.status())
      {
         animation = 10;
         undo.status(false);
         if (undoList.empty())
            return;
         Move m = undoList.top(); undoList.pop();
         app.gomoku.undo(m);
         if (app.gomoku.currentPlayer() == WHITE  && app.p1level
               || app.gomoku.currentPlayer() == BLACK  && app.p2level)
         {
            if (undoList.empty())
               return;
            m = undoList.top(); undoList.pop();
            app.gomoku.undo(m);
         }
      }

      if (menu.status())
      {
         menu.status(false);
         app.changeView("menu");
      }
      app.force_redraw();
   }

   virtual void on_resize(int, int)
   {
      app.force_redraw();
      double w = app.rbuf_window().width();
      double h = app.rbuf_window().height();
      size = std::min(w*0.95, h*0.9);
      hshift = h - size;
      hshift -= hshift/4;
      wshift = w - size;
      wshift /= 2;
   }

   void enter()
   {
      app.play_music(rand()%4, 40);
   }

   virtual void on_idle()
   {
      if (animation)
      {
         animation--;
         app.wait_mode(false);
         usleep(1000);
         app.force_redraw();
         return;
      }

      if (app.gomoku.endOfTheGame())
      {
         app.wait_mode(true);
         return;
      }

      if (app.gomoku.currentPlayer() == WHITE && !app.p1level
            || app.gomoku.currentPlayer() == BLACK && !app.p2level)
      {
         app.wait_mode(true);
         return;
      }

      app.start_timer();
      Move m = ai_move(app.gomoku, app.gomoku.currentPlayer() == WHITE?
            app.p1level : app.p2level, callback, (void*)&app);
      app.gomoku.move(m.first, m.second);
      undoList.push(m);
      animation = app.gomoku.endOfTheGame()? 50: 20;
      app.cPoints += app.gomoku.endOfTheGame()? 1: 0;
      app.wait_mode(false);
      app.force_redraw();
      app.play_sound(
            app.gomoku.endOfTheGame()? 2:
            app.gomoku.currentPlayer() == WHITE? 0:1,
            500);
   }

   int pixToX(double x)
   {
      x -= wshift;
      if (x < 0) return -1;

      double wsize = size/(SIZE-1);
      return (x+wsize/2)/wsize;
   }
   int pixToY(double y)
   {
      y -= hshift;
      if (y < 0) return -1;

      double hsize = size/(SIZE-1);
      return (y+hsize/2)/hsize;
   }


   virtual void on_mouse_button_up(int x, int y, unsigned flags)
   {
      if (was_multi or was_move)
      {
         was_multi = false;
         was_move = false;
         return;
      }

      if (m_ctrls.on_mouse_button_up(x, y))
      {
         app.on_ctrl_change();
         app.force_redraw();
         return;
      }

      if (not app.gomoku.endOfTheGame())
         try {
            app.gomoku.move(pixToX(x), pixToY(y));
            animation = app.gomoku.endOfTheGame()? 50: 20;
            app.hPoints +=  app.gomoku.endOfTheGame()? 1: 0;
            undoList.push(Move(pixToX(x), pixToY(y)));
            app.play_sound(
                  app.gomoku.endOfTheGame()? 2:
                  app.gomoku.currentPlayer() == WHITE? 0:1,
                  500);
         } catch (MoveNotValid& m) {}
      app.force_redraw();
      app.wait_mode(false);
   }

   virtual void on_mouse_button_down(int x, int y, unsigned flags)
   {
      if (m_ctrls.on_mouse_button_down(x, y))
      {
         app.on_ctrl_change();
         app.force_redraw();
         return;
      }
      downX = x;
      downY = y;
   }

   virtual void on_mouse_move(int x, int y, unsigned flags)
   {
      if (m_ctrls.on_mouse_move(x, y, (flags & agg::mouse_left) != 0))
      {
         app.on_ctrl_change();
         app.force_redraw();
         return;
      }

      if (!flags &agg::mouse_left or was_multi)
         return;

      if ((downX-x)*(downX-x)+(downY-y)*(downY-y) < 20)
         return;

      was_move = true;
      zoom_mtx *= agg::trans_affine_translation(x-downX, y-downY);
      downX=x;
      downY=y;
      on_resize(0, 0);
      size *= zoom_mtx.scale();
      zoom_mtx.transform(&wshift, &hshift);
      app.force_redraw();
   }

   virtual void on_multi_gesture(float x, float y,
         float dTheta, float dDist, int numFingers)
   {
      was_multi = true;
      if (dDist >= 0)
      {
         x *= app.rbuf_window().width();
         y *= app.rbuf_window().height();
         y = app.rbuf_window().height()-y;
         zoom_mtx *= agg::trans_affine_translation(-x, -y);
         zoom_mtx *= agg::trans_affine_scaling((1+4*dDist));
         zoom_mtx *= agg::trans_affine_translation(x, y);
         on_resize(0, 0);
         size *= zoom_mtx.scale();
         zoom_mtx.transform(&wshift, &hshift);
         app.force_redraw();

      }
      if (dDist < -0.01)
      {
         zoom_mtx.reset();
         on_resize(0, 0);
         app.force_redraw();
      }
   }


   virtual void on_draw()
   {
      pixfmt_type pf(app.rbuf_window());;
      renderer_base_type rbase(pf);
      agg::rasterizer_scanline_aa<> ras;
      agg::scanline_u8 sl;
      ras.reset();
      rbase.clear(lgray);

      double s = size/(SIZE-1);

      int i;
      for (i = 0; i< SIZE; i++)
      {
         double x = wshift;
         double y = s*i+hshift;
         double l = size+wshift;
         rbase.blend_hline(x, y, l, lblue, 65);
      }

      for (i = 0; i< SIZE; i++)
      {
         double x = s*i+wshift;
         double y = hshift;
         double l = size+hshift;
         rbase.blend_vline(x, y, l, lblue, 64);
      }

      renderer_scanline_type ren_sl(rbase);
      for (int i = 0; i < SIZE; i++)
         for (int j = 0; j < SIZE; j++)
         {
            if (app.gomoku.getPoint(i, j) == EMPTY)
               continue;

            ren_sl.color(app.gomoku.getPoint(i, j) == WHITE?
                  agg::rgba(1, 1, 1, 0.9):
                  agg::rgba(0.1, 0.1, 0.1, 0.9));
            agg::ellipse e;
            ras.reset();

            double x = wshift+s*i;
            double y = hshift+s*j;
            double size = s*0.4;
            e.init(x, y, size, size, 128);
            ras.add_path(e);
            agg::render_scanlines(ras, sl, ren_sl);

         }
      if (animation && app.gomoku.endOfTheGame())
      {
         ren_sl.color(agg::rgba(1, 0, 0, 0.02*animation--));
         ras.reset();
         for (int i = 0; i < 5; i++)
         {
            double x = wshift+s*app.gomoku.winnigRow[i].first;
            double y = hshift+s*app.gomoku.winnigRow[i].second;
            double size = s*0.4;
            agg::ellipse e;
            e.init(x, y, size, size, 128);
            ras.add_path(e);
         }
         agg::render_scanlines(ras, sl, ren_sl);
      } else if (animation && !undoList.empty())
      {
         ren_sl.color(agg::rgba(1, 0, 0, 0.02*animation--));
         ras.reset();
         agg::ellipse e;
         Move m = undoList.top();
         double x = wshift+s*m.first;
         double y = hshift+s*m.second;
         double size = s*0.4;
         e.init(x, y, size, size, 128);
         ras.add_path(e);
         agg::render_scanlines(ras, sl, ren_sl);
      }

      double scale = app.rbuf_window().width()/400.0;
      static agg::trans_affine shape_mtx; shape_mtx.reset();
      shape_mtx *= agg::trans_affine_scaling(scale);
      shape_mtx *= agg::trans_affine_translation(0, 0);
      undo.transform(shape_mtx);
      menu.transform(shape_mtx);

      agg::render_ctrl(ras, sl, rbase, undo);
      agg::render_ctrl(ras, sl, rbase, menu);

      app.draw_text(40, 100, 15*scale, red, 1.0, "%s",
            app.gomoku.haveFive(WHITE)? "P1 (WHITE) Wins!":
            app.gomoku.haveFive(BLACK)? "P2 (BLACK) Wins!":
            app.gomoku.endOfTheGame()? "Game is a draw":
            app.gomoku.currentPlayer() == WHITE?
            "P1 (WHITE) should move":
            "P2 (BLACK) should move");
   }
private:
    App& app;
    std::stack<Move> undoList;
    int downX, downY;
    agg::button_ctrl<agg::rgba8> undo;
    agg::button_ctrl<agg::rgba8> menu;
    int animation;
    double size, wshift, hshift;
    agg::trans_affine zoom_mtx;
    bool was_multi;
    bool was_move;
};

