#ifndef GOMOKU_AI_H
#define GOMOKU_AI_H

#include "gomoku.h"
#include "ai_minmax_search.h"
#include <utility>

class Gomoku_ai: public Gomoku
{
   public:
      Gomoku_ai()
         :Gomoku()
      {
         possibleMovesCount = 0;
         winner = EMPTY;
         for (int i = 0; i< SIZE; i++)
            for (int j = 0; j< SIZE; j++)
            {
               white[i][j] = 0;
               black[i][j] = 0;
            }
      }
      Gomoku_ai(const Gomoku_ai& other)
      {
         *this = other;
         possibleMovesCount = 0;
      }

      void move(Move m)
      {
         return move(m.first, m.second);
      }

      void undo(Move m)
      {
         Gomoku::undo(m);
         int x = m.first; int y = m.second;
         for (int i = 0; i< SIZE; i++)
            for (int j = 0; j< SIZE; j++)
            {
               white[i][j] = 0;
               black[i][j] = 0;
               scorePoint(i, j, WHITE);
               scorePoint(i, j, BLACK);
            }
         winner = EMPTY;
      }

      int score(int x, int y, int i, int j, Point p)
      {
         int f = 1;
         int b = 1;
         while (inRange(x+i*f, y+j*f) && points[x+i*f][y+j*f] == p) f++;
         while (inRange(x-i*b, y-j*b) && points[x-i*b][y-j*b] == p) b++;
         int ff = (f == 1)? 0:f;
         bool fempty = inRange(x+i*ff, y+j*ff) && points[x+i*ff][y+j*ff] == EMPTY;
         bool bempty = inRange(x-i*b, y-j*b)   && points[x-i*b][y-j*b] == EMPTY;
         return 2*(f+b-1) + (fempty&&bempty? 1:0);
      }

      void scorePoint(int x, int y, Point p)
      {
         if (!inRange(x, y))
            return;

         char (*scores)[SIZE] = p==BLACK? black: white;
         scores[x][y] = 0;

         if (points[x][y] != EMPTY)
            return;

         for (int i = -1; i<= 1; i++)
            for (int j = -1; j<= 1; j++)
            {
               int s = score(x, y, i, j, p);
               if (s > scores[x][y]) scores[x][y] = s;
            }
      }

      void move(int x, int y)
      {
         Point p = currentPlayer();
         Gomoku::move(x, y);
         char (*scores)[SIZE] = p==BLACK? black: white;
         if (scores[x][y] >= 10)
         {
            winner = p;
         }
           
         black[x][y] = 0;
         white[x][y] = 0;

         for (int i = -1; i<= 1; i++)
            for (int j = -1; j<= 1; j++)
            {
               if (!i && !j) continue;

               int f = 1;
               while (inRange(x+i*f, y+j*f) && points[x+i*f][y+j*f] == WHITE)
                  f++;
               int xi = x+i*f; int yj = y+j*f;
               scorePoint(xi, yj, WHITE);

               f = 1;
               while (inRange(x+i*f, y+j*f) && points[x+i*f][y+j*f] == BLACK)
                  f++;
               xi = x+i*f; yj = y+j*f;
               scorePoint(xi, yj, BLACK);
            }

         //for (int i = 0; i< SIZE; i++)
         //   for (int j = 0; j< SIZE; j++)
         //      printf("%d%c", (int)scores[i][j], j == SIZE-1? '\n': ' ');
      }

      int evaluate(int level)
      {
         Point p = (moveNr-level)%2? BLACK: WHITE;
         Point o = (p == BLACK)?     WHITE: BLACK;
         bool pmove = (level%2 == 0);

         if (winner == p)
            return 100;
         if (winner == o)
            return -100;

         int whiteGood = 0;
         int blackGood = 0;
         for (int i = 0; i< SIZE; i++)
            for (int j = 0; j< SIZE; j++)
            {
               int b = black[i][j];
               int w = white[i][j];
               if (w >= 9)
                  whiteGood++;
               if (b >= 9)
                  blackGood++;
            }
         int pGood = (p == WHITE)? whiteGood: blackGood;
         int oGood = (o == WHITE)? whiteGood: blackGood;
         return pGood - oGood;
      }


      typedef Move* iterator;
      iterator begin() { updatePossible(); return possibleMoves; }
      iterator end() { return possibleMoves + possibleMovesCount; }

   private:
      char black[SIZE][SIZE];
      char white[SIZE][SIZE];
      Move bestMoves[10];
      Move goodMoves[10];
      Move okMoves[10];
      Move sosoMoves[10];
      Move *possibleMoves;
      int possibleMovesCount;
      int bestMovesCount;
      int goodMovesCount;
      int okMovesCount;
      int sosoMovesCount;
      Point winner;

      void updatePossible()
      {
         possibleMovesCount = 0;
         bestMovesCount = 0;
         goodMovesCount = 0;
         okMovesCount = 0;
         sosoMovesCount = 0;

         if (winner != EMPTY)
            return;

         for (int i = 0; i< SIZE; i++)
         {
            for (int j = 0; j< SIZE; j++)
            {
               int b = black[i][j];
               int w = white[i][j];

               if ((b >= 9 || w >= 9) && bestMovesCount < 10)
               {
                  /* 5 in a row or 4 unprotected possible */
                  bestMoves[bestMovesCount++] = Move(i, j);
               } else if ((b >= 2*3+1 || w >= 2*3+1) && goodMovesCount < 10)
               {
                  /* 3 in a row unprotected or 4 in a row possible */
                  goodMoves[goodMovesCount++] = Move(i, j);
               } else if ((b >= 5 || w >= 5) && okMovesCount < 5)
               {
                  /* 3 in a row possible */
                  okMoves[okMovesCount++] = Move(i, j);
               } else if ((b > 3 || w > 3) && sosoMovesCount < 5)
               {
                  /* some move possible */
                  sosoMoves[sosoMovesCount++] = Move(i, j);
               }
            }
         }
         if (bestMovesCount)
         {
            possibleMovesCount = bestMovesCount;
            possibleMoves = bestMoves;
            return;
         }
         if (goodMovesCount)
         {
            possibleMovesCount = goodMovesCount;
            possibleMoves = goodMoves;
            return;
         }
         if (okMovesCount)
         {
            possibleMovesCount = okMovesCount;
            possibleMoves = okMoves;
            return;
         }
         if (sosoMovesCount)
         {
            possibleMovesCount = sosoMovesCount;
            possibleMoves = sosoMoves;
            return;
         }

         if (moveNr == 0)
         {
            possibleMovesCount = 1;
            sosoMoves[0] =  Move(8,8);
            possibleMoves = sosoMoves;
         }
      }
};


Move ai_move(Gomoku_ai b, int level, CbFun, void* cbFunData);

#endif
