#ifndef GOMOKU_H
#define GOMOKU_H

#include <stdio.h>
#include <stack>

#define SIZE 19

typedef char Point;
#define EMPTY 0
#define BLACK 1
#define WHITE 2

struct Move {
   char first;
   char second;
   Move(int x, int y): first(x), second(y) {}
   Move() {}
   int x() {return first;}
   int y() {return second;}
};

class MoveNotValid {};

class Gomoku
{
   protected:
   Point points[SIZE][SIZE];
   int moveNr; 
   int scoreP0, scoreP1;

   public:
   Gomoku():
      scoreP0(0), scoreP1(0), moveNr(0)
   {
      for (int i = 0; i < SIZE; i++)
         for (int j = 0; j < SIZE; j++)
            points[i][j] = EMPTY;
   }
   Gomoku(const Gomoku& other)
   {
      *this = other;
   }

   int getPoint(int x, int y) {return points[x][y];}
   int getScoreP1() { return scoreP0; }
   int getScoreP2() { return scoreP1; }
   bool endOfTheGame() {return haveFive(BLACK) || haveFive(WHITE) || noMoreEmpty(); }

   bool inRange(int x, int y)
   {
      if (x < 0 || y < 0 || x >= SIZE || y >= SIZE)
         return false;
      return true;
   }


   bool noMoreEmpty()
   {
      for (int i = 0; i < SIZE; i++)
         for (int j = 0; j < SIZE; j++)
            if (points[i][j] == EMPTY) return false;
      return true;
   }
   bool haveFive(int x, int y, Point p)
   {
      for (int i = -1; i <= 1; i++)
         for (int j = -1; j <= 1; j++)
            if ((i || j) && inRange(x+4*i, y+4*j)
                  && points[x+0*i][y+0*j] == p
                  && points[x+1*i][y+1*j] == p
                  && points[x+2*i][y+2*j] == p
                  && points[x+3*i][y+3*j] == p
                  && points[x+4*i][y+4*j] == p)
         {
            winnigRow[0] = Move(x+0*i, y+0*j);
            winnigRow[1] = Move(x+1*i, y+1*j);
            winnigRow[2] = Move(x+2*i, y+2*j);
            winnigRow[3] = Move(x+3*i, y+3*j);
            winnigRow[4] = Move(x+4*i, y+4*j);
            return true;
         }
      return false;
   }
   bool haveFive(Point p)
   {
      for (int i = 0; i < SIZE; i++)
         for (int j = 0; j < SIZE; j++)
         {
            if (haveFive(i, j, p))
               return true;
         }
      return false;
   }

   bool move_valid(int x, int y)
   {
      if (x < 0 || x >= SIZE || y < 0 || y >= SIZE
            || points[x][y] != EMPTY)
         throw MoveNotValid();
   }

   Point currentPlayer()
   {
      return (moveNr%2)? BLACK: WHITE;
   }

   int* currentPlayerScore()
   {
      return (moveNr%2 == 0)? &scoreP0 : &scoreP1;
   }

   void move(int x, int y)
   {
      move_valid(x, y);
      points[x][y] = currentPlayer();
      moveNr++;
   }

   void undo(Move m)
   {
      points[m.first][m.second] = EMPTY;
      if (moveNr) moveNr--;
   }

   Move winnigRow[5];
};

#endif
